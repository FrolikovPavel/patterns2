﻿// Порождающие паттерны   - Фабричный метод
// Структурные паттерны   - Адаптер
// Поведенческие паттерны - Состояние

using System.Collections.Generic;
using System.Text;
using System;

// Фабричный метод
public abstract class File
{
  public string Type { get; set; }
  public string Name { get; set; }
  public string Directory { get; set; }
  public string Data { get; set; }
}

public class TextFile : File
{
  public string Encoding { get; set; }
}

public class BinaryFile : File
{
  public string Header { get; set; }
}

public abstract class FileCreator
{
  public abstract File CreateFile(string name, string directory, string data, string specific);
}

public class TextFileCreator : FileCreator
{
  private TextFile textFile;
  public override File CreateFile(string name, string directory, string text, string encoding)
  {
    textFile = new TextFile();
    textFile.Type = "Text";
    textFile.Name = name + ".txt";
    textFile.Directory = directory;
    textFile.Data = text;
    textFile.Encoding = encoding;
    Console.WriteLine($"Создан текстовый файл {textFile.Name} в директории {textFile.Directory}");
    Console.WriteLine($"Содержание файла: \"{textFile.Data}\"");
    Console.WriteLine($"Кодировка: {textFile.Encoding}\n");
    return textFile;
  }
}

public class BinaryFileCreator : FileCreator
{
  private BinaryFile binaryFile;
  public override File CreateFile(string name, string directory, string binary_data, string header)
  {
    binaryFile = new BinaryFile();
    binaryFile.Type = "Binary";
    binaryFile.Name = name + ".bin";
    binaryFile.Directory = directory;
    binaryFile.Header = Convert.ToBase64String(Encoding.UTF8.GetBytes(header));
    binaryFile.Data = Convert.ToBase64String(Encoding.UTF8.GetBytes(binary_data));
    Console.WriteLine($"Создан бинарный файл {binaryFile.Name} в директории {binaryFile.Directory}");
    Console.WriteLine($"Заголовок файла: \"{binaryFile.Header}\"");
    Console.WriteLine($"Содержание файла: \"{binaryFile.Data}\"\n");
    return binaryFile;
  }
}

// Интерфейс для изменения файлов
public interface IFileUpdate
{
  void ChangeFileContent(string new_content);
}

// Класс для изменения текстовых файлов
public class UpdateTextFile
{
  public void ChangeTextFileContent(File text_file,  string new_text)
  {
    text_file.Data = new_text;
    Console.WriteLine($"Строка \"{text_file.Data}\" записана в текстовый файл {text_file.Name}");
  }
}

// Класс для изменения бинарных файлов
public class UpdateBinaryFile
{
  public void ChangeBinaryFileContent(File binary_file, string new_data)
  {
    binary_file.Data = Convert.ToBase64String(Encoding.UTF8.GetBytes(new_data));
    Console.WriteLine($"Строка \"{binary_file.Data}\" записана в бинарный файл {binary_file.Name}");
  }
}

// Адаптер для текстовых файлов
public class TextFileAdapter : IFileUpdate
{
  private readonly File _textFile;
  private UpdateTextFile _updateTextFile = new UpdateTextFile();

  public TextFileAdapter(File textFile)
  {
    _textFile = textFile;
  }

  public void ChangeFileContent(string new_text)
  {
    _updateTextFile.ChangeTextFileContent(_textFile, new_text);
  }
}

// Адаптер для бинарных файлов
public class BinaryFileAdapter : IFileUpdate
{
  private readonly File _binaryFile;
  private UpdateBinaryFile _updateBinaryFile = new UpdateBinaryFile();

  public BinaryFileAdapter(File binaryFile)
  {
    _binaryFile = binaryFile;
  }

  public void ChangeFileContent(string new_data)
  {
    _updateBinaryFile.ChangeBinaryFileContent(_binaryFile, new_data);
  }
}

// Состояние
public interface State
{
  void Open(FileActions fileActions, File file);
  void Close(FileActions fileActions, File file);
  void Read(FileActions fileActions, File file);
  void Write(FileActions fileActions, File file, string data);
}

public class FileActions
{
  private State _state;
  private File _file;
  public FileActions(File file)
  {
    _state = new ClosedState();
    _file = file;
  }
  public void ChangeFile(File new_file)
  {
    _state.Close(this, _file);
    _file = new_file;
  }
  public void SetState(State state)
  {
    _state = state;
  }
  public void OpenFile()
  {
    _state.Open(this, _file);
  }
  public void CloseFile()
  {
    _state.Close(this, _file);
  }
  public void ReadFile()
  {
    _state.Read(this, _file);
  }
  public void WriteFile(string data)
  {
    _state.Write(this, _file, data);
  }
}

public class OpenState : State
{
  public void Open(FileActions fileActions, File file)
  {
    Console.WriteLine("Файл уже открыт");
  }
  public void Close(FileActions fileActions, File file)
  {
    Console.WriteLine($"Закрытие файла {file.Name}\n");
    fileActions.SetState(new ClosedState());
  }
  public void Read(FileActions fileActions, File file)
  {
    Console.WriteLine("Чтение из файла");
    Console.WriteLine($"Содержимое файла {file.Name} : \"{file.Data}\"");
  }
  public void Write(FileActions fileActions, File file, string data)
  {
    Console.WriteLine("Запись в файл");
    if (file.Type == "Text")
    {
      file.Data = data;
      Console.WriteLine($"Строка \"{file.Data}\" записана в текстовый файл {file.Name}");
    }
    else if (file.Type == "Binary")
    {
      file.Data = Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
      Console.WriteLine($"Строка \"{file.Data}\" записана в бинарный файл {file.Name}");
    }
  }
}

public class ClosedState : State
{
  public void Open(FileActions fileActions, File file)
  {
    Console.WriteLine($"Открытие файла {file.Name}");
    fileActions.SetState(new OpenState());
  }
  public void Close(FileActions fileActions, File file)
  {
    Console.WriteLine("Файл уже закрыт");
  }
  public void Read(FileActions fileActions, File file)
  {
    Console.WriteLine("Файл закрыт, чтение невозможно");
  }
  public void Write(FileActions fileActions, File file, string data)
  {
    Console.WriteLine("Файл закрыт, запись невозможна");
  }
}

//
public class Program
{
  public static void Main()
  {
    // Использование фабричного метода
    TextFileCreator textFileCreator = new TextFileCreator();
    BinaryFileCreator binaryFileCreator = new BinaryFileCreator();
    File textFile = textFileCreator.CreateFile("text_file", "C:\\text_files", "Text", "UTF-8");
    File binaryFile = binaryFileCreator.CreateFile("binary_file", "C:\\binary_files", "Binary data", "Binary file header");
    
    // Использование адаптеров
    TextFileAdapter textFileAdapter = new TextFileAdapter(textFile);
    BinaryFileAdapter binaryFileAdapter = new BinaryFileAdapter(binaryFile);
    textFileAdapter.ChangeFileContent("Новый текст");
    binaryFileAdapter.ChangeFileContent("Новые бинарные данные");
    Console.WriteLine();

    // Использование состояния
    FileActions fileActions = new FileActions(textFile);
    fileActions.OpenFile();
    fileActions.ReadFile();
    fileActions.ChangeFile(binaryFile);
    fileActions.OpenFile();
    fileActions.ReadFile();
    fileActions.WriteFile("qwerty123");
    fileActions.ReadFile();
    fileActions.CloseFile();
    fileActions.ReadFile();

    Console.ReadLine();
  }
}
